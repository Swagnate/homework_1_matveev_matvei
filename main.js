#! Matveev Matvei.

/**
 * Задание 1.
 *
 * Напишите функцию, которая принимает на вход массив строк
 * и возвращает только те их них что короче 20 символов
 *
 * [
 *   'Я короткая строка',
 *   'Я вроде бы тоже короткая',
 *   'А я длинная строка'
 * ] => [
 *   'Я короткая строка',
 *   'А я длинная строка'
 * ]
 *
 * Используем: filter
 */

// РЕШЕНИЕ:

let arr = [
    'Я короткая строка',
    'Я вроде бы тоже короткая',
    'А я длинная строка'
];

let shortArr = arr.filter(arr => arr.length < 20);

console.log(shortArr);


/**
 * Задание 2 DONE
 *
 * Напишите функцию, которая принимает на вход следующие данные:
 *
 * [
 *   { name: 'shark', likes: 'ocean' },
 *   { name: 'turtle', likes: 'pond' },
 *   { name: 'otter', likes: 'fish biscuits' }
 * ]
 *
 * И возвращает массив строк:
 *
 * [ 'shark likes ocean', 'turtle likes pond', 'otter likes fish biscuits' ]
 *
 * Используем: map
 */

// РЕШЕНИЕ:

// 1-вар. =>

let obj = [
    { name: 'shark', likes: 'ocean' },
    { name: 'turtle', likes: 'pond' },
    { name: 'otter', likes: 'fish biscuits' }
];

const func = array => obj.map(({ name, likes }) => `${name} likes ${likes}`);

console.log(func());

// 2-вар. с помощью forEach.

function getLikes(animals) {
    const result = [];

    animals.forEach(
        function (animal) {
            result.push(`${animal.name} likes ${animal.likes}`);
        }
    );

    return result;
}

// 3-вар. с помощью map.

function getLikes(animals) {
    return animals.map(
        function (animal) {
            return `${animal.name} likes ${animal.likes}`;
        }
    );
}

console.log(getLikes([
    { name: 'shark', likes: 'ocean' },
    { name: 'turtle', likes: 'pond' },
    { name: 'otter', likes: 'fish biscuits' }
]));


/**
 * Задание 7
 *
 * Напишите функцию, которая возвращает наименьшее значение массива
 * [1, 2, 3, 4] => 1
 *
 * Используем: оператор ... и Math.min или for
 */

// РЕШЕНИЕ:

// 1-вар.

function getMin(arr) {
    let min = arr[0];

    for (let i = 0; i < arr.lenght; i++) {
        if (arr[i] < min) {
            min = arr[i];
        }
    }
    return min;
}

console.log(getMin([1, 2, 3, 4]))

// 2-вар. с функцией Math.

function getMin(arr) {
    return Math.min(...arr);
}
console.log(getMin([1, 2, 3, 4]))

// 3-вар. =>

const getMin = arr => Math.min(...arr);
console.log(getMin([1, 2, 3, 4]))


/**
* Задание 8 DONE
*
* Реализовать функцию, которая принимает на вход 2 аргумента: массив чисел и множитель,
* а возвращает массив исходный массив, каждый элемент которого был умножен на множитель:
*
* [1,2,3,4], 5 => [5,10,15,20]
*
* Используем: map
*/

// РЕШЕНИЕ:

// 1-вар. без  функции map

function mulNumbers(arr, mul) {
    const result = [];

    for (let i = 0; i < arr.length; i++) {
        result.push(arr[i] * mul)
    }

    return result;
}

mulNumbers([1, 2, 3, 4], 5);
console.log(mulNumbers([1, 2, 3, 4], 5));

// 2-вар. с map

function mulNumbers2(arr, mul) {
    return arr.map(
        function (value) {
            return value * mul
        }
    );
}

mulNumbers2([1, 2, 3, 4], 5)

// 3-вар. =>

const mulNumbers2 = (arr, mul) => arr.map((value) => value * mul);

mulNumbers2([1, 2, 3, 4], 5)


/**
* Задание 9
*
* Реализовать функцию, на входе которой объект следующего вида:
* {
* firstName: 'Петр',
* secondName: 'Васильев',
* patronymic: 'Иванович'
* }
* на выходе строка с сообщением 'ФИО: Петр Иванович Васильев'
*/

// РЕШЕНИЕ:

// 1-вар.

function getFullName(user) {
    const fullName = 'ФИО: ' + user.firstName + ' ' + user.patronimic + ' ' + user.secondName;

    return fullName;
}

// 2-вар.

function getFullName(user) {
    return `ФИО: ${user.firstName} ${user.patronimic} ${user.secondName}`
}

// 3-вар. =>

const getFullName = user => `ФИО: ${user.firstName} ${user.patronimic} ${user.secondName}`

console.log(getFullName({
    firstName: 'Петр',
    secondName: 'Васильев',
    patronimic: 'Иванович'
}))


/**
* Задание 10
*
* Напишите функцию, которая принимает на вход 2 объекта
* и возвращает 1 с общими свойствами
*
* { name: 'Алиса' }, { age: 11 } => { name: 'Алиса', age: 11 }
*
* Используем: ...    Если не используем ... то вставляется обьект целиком со скобками
* а если используем ... то вставляется внутренеее содержимое обьекта
*/

// РЕШЕНИЕ:

// 1-вар.

function getMergedObjects(obj1, obj2) {
    return { ...obj1, ...obj2 }
}

// 2-вар.

function getMergedObjects(obj1, obj2) {
    const result = {};

    for (const key in obj1) {
        result[key] = obj1[key];
    }

    for (const key in obj2) {
        result[key] = obj2[key];
    }

    return result;
}

// 3-вар.

function getMergedObjects(obj1, obj2) {
    const result = {};
    result.name = obj1.name;
    result.age = obj2.age;

    return result;

}

console.log(getMergedObjects({ name: 'Алиса' }, { age: 11 }))


/**
* Задание 15
*
* Реализовать функцию, на входе которой массив чисел, на выходе массив уникальных значений
* [1, 2, 2, 4, 5, 5] => [1, 2, 4, 5]
*
* Используем: reduce, indexOf
*/

// РЕШЕНИЕ:

// 1-вар.

[1, 2, 2, 4, 5, 5].reduce(
    function (acc, value, index, arr) {
        return acc.indexOf(value) === -1 ? [...acc, value] : acc;
    },
    []
)
// 2-вар. =>

[1, 2, 2, 4, 5, 5].reduce(
    (acc, value) => acc.indexOf(value) === -1 ? [...acc, value] : acc,
    []
)

// 1-вар. =>

const getUniques = arr = arr.reduce(
    (acc, value) => acc.indexOf(value) === -1 ? [...acc, value] : acc,
    []
);

getUniques([1, 2, 2, 4, 5, 5]);


/**
 * Задание 5
 *
 * Напишите функцию, которая возвращает нечетные значения массива.
 * [1,2,3,4] => [1,3]
 *
 * Используем: reduce
 */

// РЕШЕНИЕ:

const oddNumber = arr => arr.reduce((acc, value) => (value % 2 != 0 ? [...acc, value] : acc), [])

const data = [1, 2, 3, 4]

console.log(oddNumber(data))


/**
 * Задание 2
 *
 * Напишите функцию, которая принимает на вход данные из корзины в следующем виде:
 *
 * [
 *   { price: 10, count: 2},
 *   { price: 100, count: 1},
 *   { price: 2, count: 5},
 *   { price: 15, count: 6},
 * ]
 * где price это цена товара, а count количество.
 *
 * Функция должна вернуть итоговую сумму по данному заказу.
 *
 * Используем: reduce
 */

// РЕШЕНИЕ:

let arr = [
    { price: 10, count: 2 },
    { price: 100, count: 1 },
    { price: 2, count: 5 },
    { price: 15, count: 6 },
]

arr.map(({ price, count }) => price * count).reduce((acc, val) => acc + val)


/**
 * Задание 4
 *
 * Реализовать функцию, на входе которой число с ошибкой, на выходе строка с сообщением
 * 500 => Ошибка сервера
 * 401 => Ошибка авторизации
 * 402 => Ошибка сервера
 * 403 => Доступ запрещен
 * 404 => Не найдено
 *
 * Используем: switch case
 */

// РЕШЕНИЕ:

let errorNum = 500;

switch (errorNum) {
    case 500:
    case 402:
        console.log('Ошибка сервера')
        break;
    case 401:
        console.log('Ошибка авторизации')
        break;
    case 403:
        console.log('Доступ запрещен')
        break;
    case 404:
        console.log('Не найдено')
        break;
    default:
        console.log('I love JS')
}


/**
 * Задание 5 DONE
 *
 * Напишите функцию, которая возвращает 2 наименьших значение массива
 * [4,3,2,1] => [1,2]
 *
 * Используем: .sort()
 */

// РЕШЕНИЕ:

let arr = [4, 3, 2, 1],
    res = arr.sort((a, b) => a - b).slice(0, 2);

console.log(res)

/**
 * Задание 8
 *
 * Реализовать функцию, которая принимает на вход 2 аргумента: массив и франшизу,
 * а возвращает строку с именнами героев разделенных запятой:
 *
 * [
 *    {name: “Batman”, franchise: “DC”},
 *    {name: “Ironman”, franchise: “Marvel”},
 *    {name: “Thor”, franchise: “Marvel”},
 *    {name: “Superman”, franchise: “DC”}
 * ],
 * Marvel
 * => Ironman, Thor
 *
 * Используем: filter, map, join
 */

// РЕШЕНИЕ:

const givenArr = [{ name: 'Batman', franchise: 'DC' },
{ name: 'Ironman', franchise: 'Marvel' },
{ name: 'Thor', franchise: 'Marvel' },
{ name: 'Superman', franchise: 'DC' }],

    heroFunction = function (array, franchise) {
        return franchise +
            ' => ' +
            array.filter(string => string.franchise === franchise)
                .map(string => string.name)
                .join(', ');
    },

    result = heroFunction(givenArr, 'Marvel');

console.log(result);

// END